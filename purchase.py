# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-TODAY OpenERP s.a. (<http://www.openerp.com>).
#    Copyright (C) 2012-TODAY Mentis d.o.o. (<http://www.mentis.si/openerp>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv, fields
from openerp.tools.translate import _
from collections import defaultdict
import datetime
import logging
_logger = logging.getLogger(__name__)

class purchase_order(osv.Model):
    _inherit = "purchase.order"
    _columns = {
        'invoice_anticipo_ids': fields.one2many('account.invoice', 'purchase_anticipo_id', 'Facturas Anticipos'),
        'voucher_anticipo_ids': fields.one2many('account.voucher', 'purchase_anticipo_id', 'Pagos Anticipos'),
    }


