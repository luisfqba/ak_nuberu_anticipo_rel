from openerp.osv import osv, fields

class account_voucher(osv.Model):
    _inherit = 'account.voucher'

    _columns = {
        'anticipo': fields.boolean('Es Anticipo'),
        'purchase_anticipo_id': fields.many2one('purchase.order', 'Pedido Anticipo'),
    }
