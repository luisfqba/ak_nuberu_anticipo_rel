from openerp.osv import osv, fields

class account_invoice(osv.Model):
    _inherit = 'account.invoice'

    _columns = {
        'anticipo': fields.boolean('Es Anticipo'),
        'purchase_anticipo_id': fields.many2one('purchase.order', 'Pedido Anticipo'),
    }
